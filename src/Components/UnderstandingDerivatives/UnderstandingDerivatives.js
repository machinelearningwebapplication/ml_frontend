import React from "react";
import "./UnderstandingDerivatives.scss";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import RangeSlider from "../RangeSlider/RangeSlider";
// import Navbar from "../Navbar/Navbar";

let dataset = [
  {
    x: 2.0,
    y: 0.2,
    prediction: 0,
  },
  {
    x: 1.4,
    y: 0.2,
    prediction: 0,
  },
  {
    x: 3,
    y: 1.1,
    prediction: 1,
  },
  {
    x: 4.1,
    y: 1.3,
    prediction: 1,
  },
  {
    x: 5.4,
    y: 2.3,
    prediction: 2,
  },
  {
    x: 5.1,
    y: 1.8,
    prediction: 2,
  },
];

let values = {
  w1: Math.random().toFixed(4),
  w5: Math.random().toFixed(4),
  b1: Math.random().toFixed(4),
  b2: Math.random().toFixed(4),
};
class UnderstandingDerivatives extends React.Component {
  constructor(props) {
    super(props);
    this.state = { refresh: false };
  }
  runNeuralNet() {
    values["n1"] = this.computeNode(
      dataset[0]["x"],
      values["w1"],
      values["b1"]
    );
    values["sn1"] = this.sigmoid(values["n1"]);
    values["n2"] = this.computeNode(values["sn1"], values["w5"], values["b2"]);
    values["sn2"] = this.sigmoid(values["n2"]);
    values["loss"] = Math.pow(1.0 - values["sn2"], 2);

    console.log("Layer 1 -->");
  }
  nextStep() {
    let change = 0;
    change = this.backPropogation(values["sn1"], values["n2"], values["sn2"]);
    values["w5"] = parseFloat(values["w5"]) + parseFloat(change[0]);
    values["b2"] = parseFloat(values["b2"]) + parseFloat(change[1]);
    this.backPropogation(dataset[0]["x"], values["n1"], values["sn1"]);
    values["w1"] = parseFloat(values["w1"]) + parseFloat(change[0]);
    values["b1"] = parseFloat(values["b1"]) + parseFloat(change[1]);
    this.runNeuralNet();
    this.setState({ refresh: !this.state.refresh });
  }
  componentWillMount() {
    this.runNeuralNet();
    //console.log(this.computeNode(dataset[0]["x"], values["w1"], values["b1"]));
  }
  computeNode(ip, weight, bias) {
    let dotProduct = ip * weight;
    let sum = parseFloat(dotProduct) + parseFloat(bias);

    // console.log("ip: " + ip);
    // console.log("weight: " + weight);
    // console.log("bias: " + bias);
    // console.log("ip * weight + bias: " + sum);
    // console.log("sigmoid(ip * weight + bias): " + this.sigmoid(sum));

    return sum;
  }
  sigmoid(t) {
    return 1 / (1 + Math.pow(Math.E, -t));
  }
  derivativeSigmoid(f) {
    return f * (1 - f);
  }
  handleChange(event) {
    values[event.target.id] = event.target.value;

    // if (event.target.id === "w1") {
    //   values["w2"] = 2 * event.target.value;
    // }
    if (event.target.id !== "loss") {
      this.runNeuralNet();
    }

    //console.log(values);
    this.setState({ refresh: !this.state.refresh });
  }
  backPropogation(ip, product, sn2) {
    let change = ip * this.derivativeSigmoid(product) * (2 * (1 - sn2));
    let change2 = this.derivativeSigmoid(product) * (2 * (1 - sn2));
    return [change, change2];
  }
  calculateDerivative(ip, sn2) {}
  render() {
    return (
      <React.Fragment>
        {/* <Navbar /> */}
        <div
          className={
            "UnderstandingDerivatives-container " + this.props.className
          }
        >
          Neural Net:
          <br />
          <h5>
            Input:
            <RangeSlider
              id="ip1"
              value={dataset[0]["x"]}
              onChange={(e) => this.handleChange(e)}
            />
            {dataset[0]["x"]}
          </h5>
          <RangeSlider
            id="w1"
            value={values["w1"]}
            onChange={(e) => this.handleChange(e)}
          />
          <RangeSlider
            id="b1"
            value={values["b1"]}
            onChange={(e) => this.handleChange(e)}
          />
          <h5>
            Node 1:
            <RangeSlider
              id="n1"
              value={values["n1"]}
              onChange={(e) => this.handleChange(e)}
            />
            {values["n1"]}
          </h5>
          <hr />
          <h5>
            Input to Node 2:
            <RangeSlider
              id="ip2"
              value={values["sn1"]}
              onChange={(e) => this.handleChange(e)}
            />
            {values["sn1"]}
          </h5>
          <RangeSlider
            id="w5"
            value={values["w5"]}
            onChange={(e) => this.handleChange(e)}
          />
          <RangeSlider
            id="b2"
            value={values["b2"]}
            onChange={(e) => this.handleChange(e)}
          />
          <h5>
            Node 2:
            <RangeSlider
              id="n3"
              value={values["sn2"]}
              onChange={(e) => this.handleChange(e)}
            />
            {values["sn2"]}
          </h5>
          <hr></hr>
          <h5>
            Loss:
            <RangeSlider
              id="loss"
              min="0"
              max="1"
              value={values["loss"]}
              onChange={(e) => this.handleChange(e)}
            />
            {values["loss"]}
          </h5>
        </div>
        <div onClick={() => this.nextStep()}>Back Propogate Once</div>
        {/* <Footer></Footer> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sessionInfo: { ...state.sessionInfo },
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    testAction: (input) =>
      dispatch({
        type: "testAction",
        value: input,
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(UnderstandingDerivatives));
