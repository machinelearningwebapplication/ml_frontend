import React from "react";
import "./Navbar.scss";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Table from "../Table/Table";
import Tr from "../Tr/Tr";
import Td from "../Td/Td";
import Popup from "../Popup/Popup";
import PopupSection from "../PopupSection/PopupSection";
import InputContainer from "../InputContainer/InputContainer";
import InputString from "../InputString/InputString";
import Button from "../Button/Button";
import fullLogo from "../../Images/logo-01.png";
import { Animated } from "react-animated-css";

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { changePasswordPopupStatus: false };
  }
  changePasswordPopupToggle() {
    this.setState({
      changePasswordPopupStatus: !this.state.changePasswordPopupStatus
    });
  }
  sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

  mobileNavToggle() {
    if (window.innerWidth < 800) {
      if (
        document.getElementsByClassName("navigation")[0].style.left === "0px"
      ) {
        document.getElementsByClassName("navigation")[0].style.left = "-100%";
      } else {
        document.getElementsByClassName("navigation")[0].style.left = "0px";
      }
    }
  }
  gotopage(page) {
    if (page === "/") {
      localStorage.clear();
      this.props.form["loginForm"]["error"]["message"] = "";
    }
    // Usage!
    // if (page.toLowerCase() !== "/forms") {
    //   this.props.history.push("forms ");
    // } else {
    //   this.props.history.push("clients");
    // }
    if (window.location.pathname !== page) {
      this.props.history.push(page);
    } else {
      console.log("reloading");
      const oldpage = this.props.location.pathname;
      this.props.history.replace(`/reload`);
      setTimeout(() => {
        this.props.history.replace(oldpage);
      });
    }

    this.mobileNavToggle();
  }
  componentWillMount() {
    if (this.props.sessionInfo.isAuthenticted === false) {
      this.props.history.push("/");
    }
  }
  componentDidUpdate() {
    // callBackend(){
    //   //save token in backen
    // }
    //    console.log(document);
    //alert("Finding Token");
    if (
      document.getElementById("js-token").innerHTML !== "" &&
      document.getElementById("js-token").innerHTML !== null &&
      document.getElementById("js-token").innerHTML !== undefined &&
      this.props.sessionInfo.isAuthenticted === true
    ) {
      //alert("Found Token " + document.getElementById("js-token").innerHTML);
      this.fetchToken();
    }
  }
  fetchToken() {
    //  alert("sending Token " + document.getElementById("js-token").innerHTML);
    //alert("Sending Token " + document.getElementById("js-token").innerHTML);
    fetch("https://api.formdesk.in/registerToken", {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      credentials: "same-origin", // include, *same-origin, omit
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.sessionInfo.token
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *client
      body: JSON.stringify({
        email: this.props.sessionInfo.currentLoginEmail,
        token: document.getElementById("js-token").innerHTML
      })
    });
  }
  submitForm(form) {
    this.props.InputChangeHandle({
      form: form,
      name: ""
    });

    if (this.props.form[form]["cansubmit"] === true) {
      let email = this.props.sessionInfo.currentLoginEmail;
      let newPassword = this.props.form[form]["inputBox"]["newPassword"][
        "value"
      ];
      let confirmPassword = this.props.form[form]["inputBox"][
        "confirmPassword"
      ]["value"];
      if (newPassword === confirmPassword) {
        let password = newPassword;
        fetch("https://api.formdesk.in/users", {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, *cors, same-origin
          cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          credentials: "same-origin", // include, *same-origin, omit
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + this.props.sessionInfo.token
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: "follow", // manual, *follow, error
          referrerPolicy: "no-referrer", // no-referrer, *client
          body: JSON.stringify({
            email: email,
            password: password
          })
        });
        fetch("https://api.formdesk.in/sendEmail", {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, *cors, same-origin
          cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          credentials: "same-origin", // include, *same-origin, omit
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + this.props.sessionInfo.token
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: "follow", // manual, *follow, error
          referrerPolicy: "no-referrer", // no-referrer, *client
          body: JSON.stringify({
            body:
              "Your password for Email Id: " +
              email +
              " on formDesk has been changed to " +
              password,
            cc: "",
            subject: "Password Change Notification",
            to: email
          })
        });
        this.props.FormError({
          form: form,
          type: "success",
          message: "Password Updated Successfully"
        });
        this.changePasswordPopupToggle();
      } else {
        this.props.FormError({
          form: form,
          type: "failure",
          message: "Passwords Do Not Match"
        });
      }
    } else {
      this.props.FormError({
        form: form,
        type: "failure",
        message: "Please fill all required details correctly"
      });
    }
  }
  renderNavbar() {
    let rows = [];
    if (this.props.sessionInfo.isAuthenticted === true) {
      if (this.props.sessionInfo.currentLoginRole === "Super Admin") {
        rows.push(
          <li
            className={
              this.props.activePage === "organisation"
                ? "organisation active"
                : "organisation"
            }
            onClick={(event) => this.gotopage("/organisation")}
          >
            Organisation
          </li>
        );
      } else {
        if (
          this.props.roles[this.props.sessionInfo.currentRoleKey]["roles"][
            "View"
          ] === true
        ) {
          rows.push(
            <li
              className={
                this.props.activePage === "roles"
                  ? "roles hide-mobile active"
                  : "roles hide-mobile"
              }
              onClick={(event) => this.gotopage("/roles")}
            >
              Roles
            </li>
          );
        }

        if (
          this.props.roles[this.props.sessionInfo.currentRoleKey]["forms"][
            "View"
          ] === true
        ) {
          rows.push(
            <li
              className={
                this.props.activePage === "forms" ? "forms active" : "forms"
              }
              onClick={(event) => this.gotopage("/forms")}
            >
              Forms
            </li>
          );
        }

        if (
          this.props.roles[this.props.sessionInfo.currentRoleKey]["users"][
            "View"
          ] === true
        ) {
          rows.push(
            <li
              className={
                this.props.activePage === "users"
                  ? "users hide-mobile active"
                  : "users hide-mobile"
              }
              onClick={(event) => this.gotopage("/users")}
            >
              Users
            </li>
          );
        }

        if (
          this.props.roles[this.props.sessionInfo.currentRoleKey]["tasks"][
            "View"
          ] === true
        ) {
          rows.push(
            <li
              className={
                this.props.activePage === "tasks" ? "tasks active" : "tasks"
              }
              onClick={(event) => this.gotopage("/tasks")}
            >
              Tasks
            </li>
          );
        }

        if (
          this.props.roles[this.props.sessionInfo.currentRoleKey]["clients"][
            "View"
          ] === true
        ) {
          rows.push(
            <li
              className={
                this.props.activePage === "clients"
                  ? "clients active"
                  : "clients"
              }
              onClick={(event) => this.gotopage("/clients")}
            >
              Clients
            </li>
          );
        }
      }
    } else {
      this.props.history.push("/");
    }
    return rows;
  }
  render() {
    let mobilenav = false;
    if (window.innerWidth < 800) {
      mobilenav = true;
    } else {
      mobilenav = false;
    }
    return (
      <div className="navigation-container">
        {mobilenav ? (
          <div className="mobile-nav">
            <div
              className="hamburger-icon"
              onClick={() => this.mobileNavToggle()}
            >
              <i className={"fa fa-bars "}></i>
            </div>
            <div
              className="active-page-title"
              onClick={(event) => this.gotopage("/" + this.props.activePage)}
            >
              {this.props.activePage.toUpperCase()}
            </div>
          </div>
        ) : null}
        <ul className="navigation">
          {mobilenav ? (
            <div
              className="close-icon"
              onClick={() => this.mobileNavToggle()}
            ></div>
          ) : null}
          <div
            className="logo-container"
            onClick={() => {
              if (this.props.sessionInfo.currentLoginRole === "Super Admin") {
                this.gotopage("/organisation");
              }
            }}
          >
            <div className="logo">
              <img src={fullLogo} alt="logo"></img>
            </div>
          </div>
          {<div id="js-token" className="hide-mobile"></div>}
          {this.renderNavbar()}

          {mobilenav ? (
            <React.Fragment>
              <li
                className="changePassword"
                onClick={() => this.changePasswordPopupToggle()}
              >
                Change Password
              </li>
              <li className="logout" onClick={(event) => this.gotopage("/")}>
                Logout
              </li>
            </React.Fragment>
          ) : (
            <li className="logout">
              Settings
              <Animated
                animationIn="slideInDown"
                animationOut="fadeOut"
                isVisible={true}
                animationInDelay="0"
                animationOutDelay="0"
                animationInDuration="1000"
                animationOutDuration="1000"
                animateOnMount={true}
              >
                <div className="dropdown-container">
                  <div
                    className="dropdown"
                    onClick={() => this.changePasswordPopupToggle()}
                  >
                    Change Password
                  </div>
                  <div
                    className="dropdown"
                    onClick={(event) => this.gotopage("/")}
                  >
                    Logout
                  </div>
                </div>
              </Animated>
            </li>
          )}

          {this.state.changePasswordPopupStatus ? (
            <Popup>
              <PopupSection
                width="449"
                title="Change Password"
                line="True"
                marginLeft="43"
                marginRight="43"
                marginTop="10"
                marginBottom="20"
                close="True"
                closeOnClick={() => this.changePasswordPopupToggle()}
              >
                <form id="changePasswordForm">
                  <Table>
                    <Tr marginTop="20">
                      <Td flex="0">
                        <InputContainer width="362" marginTop="19">
                          <InputString
                            formProp="changePasswordForm"
                            placeholder="New Password"
                            type="password"
                            icon="fa-lock"
                            name="newPassword"
                            required={true}
                            label="New Password"
                          ></InputString>
                        </InputContainer>
                      </Td>
                    </Tr>
                    <Tr>
                      <Td flex="0">
                        <InputContainer width="362" marginTop="19">
                          <InputString
                            formProp="changePasswordForm"
                            placeholder="Confirm Password"
                            type="password"
                            icon="fa-lock"
                            name="confirmPassword"
                            required={true}
                            label="Confirm Password"
                          ></InputString>
                        </InputContainer>
                      </Td>
                    </Tr>
                    <Tr>
                      <Td flex="0">
                        <Button
                          marginTop="25"
                          width="362"
                          onClick={() => this.submitForm("changePasswordForm")}
                          error={this.props.form["changePasswordForm"]["error"]}
                          id="changePasswordButton"
                        >
                          Send Password
                        </Button>
                      </Td>
                    </Tr>
                  </Table>
                </form>
              </PopupSection>
              )
            </Popup>
          ) : null}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sessionInfo: { ...state.sessionInfo },
    roles: { ...state.roles },
    form: { ...state.form },
    organisation: { ...state.organisation }
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    InputChangeHandle: (input) =>
      dispatch({
        type: "InputChangeHandle",
        value: input
      }),
    FormError: (input) =>
      dispatch({
        type: "FormError",
        value: input
      })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar));
