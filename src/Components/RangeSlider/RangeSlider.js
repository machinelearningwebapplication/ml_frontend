import React from "react";
import "./RangeSlider.scss";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
// import Navbar from "../Navbar/Navbar";

class RangeSlider extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    //document.getElementById("w1").value = 0;
  }

  render() {
    return (
      <React.Fragment>
        {/* <Navbar /> */}
        <div className={"RangeSlider-container " + this.props.className}>
          {this.props.id}:
          <input
            type="range"
            value={this.props.value}
            id={this.props.id}
            min={this.props.min === undefined ? "0" : this.props.min}
            max={this.props.max === undefined ? "1" : this.props.max}
            step={this.props.step === undefined ? "0.001" : this.props.step}
            onChange={this.props.onChange}
          />
          {this.props.value}
        </div>
        {/* <Footer></Footer> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sessionInfo: { ...state.sessionInfo },
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    testAction: (input) =>
      dispatch({
        type: "testAction",
        value: input,
      }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(RangeSlider));
