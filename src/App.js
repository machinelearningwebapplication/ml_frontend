import React from "react";
import RegistrationPage from "./Components/RegistrationPage/RegistrationPage";
import UnderstandingDerivatives from "./Components/UnderstandingDerivatives/UnderstandingDerivatives";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "./CSS/common.scss";

function App() {
  return (
    // <div className="App">
    //   <LoginPage></LoginPage>
    // </div>

    <div>
      {/* <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/error">Page Not Found</Link>
            </li>
          </ul>
        </nav> */}

      {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
      <BrowserRouter>
        <Switch>
          <Route path="/reload" component={null} key="reload" />
          {/* <Route path="/organisation" exact>
            <Organisation />
          </Route>
          <Route path="/error" exact>
            <Page404 />
          </Route>
          <Route path="/roles" exact>
            <Roles />
          </Route>
          <Route path="/forms" exact>
            <Forms />
          </Route>
          <Route path="/tasks" exact>
            <Tasks />
          </Route>
          <Route path="/users" exact>
            <UserPage />
          </Route>
          <Route path="/clients" exact>
            <Clients />
          </Route> */}
          <Route path="/" exact>
            <UnderstandingDerivatives />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
