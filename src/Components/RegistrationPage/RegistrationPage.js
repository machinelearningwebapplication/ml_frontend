import React from "react";
import "./RegistrationPage.scss";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
// import Navbar from "../Navbar/Navbar";

class RegistrationPage extends React.Component {
  componentDidMount() {}
  testClick() {
    this.props.testAction(10);
  }
  render() {
    return (
      <React.Fragment>
        {/* <Navbar /> */}
        <div className={"RegistrationPage-container " + this.props.className}>
          <div className="copyright-text" style={{ marginTop: "50px" }}>
            {this.props.sessionInfo.randomNumber}
          </div>
          <div
            onClick={() => this.testClick()}
            className="copyright-text"
            style={{ marginTop: "40px" }}
          >
            Click Me
          </div>
        </div>
        {/* <Footer></Footer> */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sessionInfo: { ...state.sessionInfo }
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    testAction: (input) =>
      dispatch({
        type: "testAction",
        value: input
      })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(RegistrationPage));
