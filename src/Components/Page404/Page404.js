import React from "react";
import "./Page404.scss";
import { Link } from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import footer from "../Footer/Footer";
import Footer from "../Footer/Footer";

class Page404 extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className={"page404-container " + this.props.className}>
          <div className="copyright-text" style={{ marginTop: "200px" }}>
            Page Under Construction
          </div>
        </div>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

export default Page404;
